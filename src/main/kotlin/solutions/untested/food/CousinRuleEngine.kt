package solutions.untested.food

import solutions.untested.food.rules.DaylyCalories
import solutions.untested.food.rules.DaylyPFCnorms

class CousinRuleEngine {
    fun testMenuStayingHome(menU: CousinMenu) = baseRules.map() { it.test(menU) }.sum()

    // proteins: https://tfs.group/en/interest-article/potrebnost-v-belkah-k-cemu-my-prisli-i-v-kakom-napravlenii-nam-dvigatsa-dalse
    // 0.83 * MASS -> ~ 55, was 75, ORFS recommends 150 (WTF?)
    // fats: https://www.championat.com/lifestyle/article-4059317-normy-belkov-zhirov-i-uglevodov--skolko-nuzhno-dlja-pohudenija-i-zdorovogo-obraza-zhizni.html
    // lets set it 60
    // carbohydrates https://fitseven.ru/pohudenie/pravilnoe-pitanie/osnovy-pitaniya-v-tsifrah
    // for 60 KG man recimmends 290, was 310. Seems near OK
    companion object {
        val baseRules = setOf(DaylyPFCnorms(55, 60, 290), DaylyCalories(2500, 3200))
    }
}