package solutions.untested.food

data class CousinMenu(val name: String, val days: List<CousinMenuDay>, val everydayBonus: CousinMenuItem) {
    fun BOM(persons: Int): List<BOMItem> {
        return days
            .flatMap { listOf(it.first, it.second, it.third, it.pocket, everydayBonus) }
            .flatMap { it.components.entries }
            .groupingBy { it.key.name }
            .fold(0, operation = { accumulator: Int, element: Map.Entry<Products, Int> ->
                accumulator + element.value * persons
            })
            .map { entry -> BOMItem(entry.key, entry.value) }
            .sortedBy { it.name }
    }

    fun weight(persons: Int) = (days.map { it.weight() }.sum() + everydayBonus.weight() * days.size) * persons
    fun weights() = days.map { it.weight() + everydayBonus.weight() }.toList()
    fun wrapPair(persons: Int) = Pair(this, persons)

    fun prettyPrint() =
        days.mapIndexed { index, cousinMenuDay ->
            """Меню на вечер $index:
            ужин:    ${cousinMenuDay.first.prettyPrint()}
            завтрак: ${cousinMenuDay.second.prettyPrint()}
            обед:    ${cousinMenuDay.third.prettyPrint()}
            перекус: ${cousinMenuDay.pocket.prettyPrint()}
        """.trimIndent()
        }.joinToString(separator = "\n")

}

data class CousinMenuDay(
    val first: CousinMenuItem,
    val second: CousinMenuItem,
    val third: CousinMenuItem,
    val pocket: CousinMenuItem,
    val name: String = ""
) {
    fun toPFCC() = first.toPFCC() + second.toPFCC() + third.toPFCC() + pocket.toPFCC()
    fun weight() = first.weight() + second.weight() + third.weight() + pocket.weight()
    fun oneDayMenu(everydayBonus: CousinMenuItem) = CousinMenu(name, listOf(this), everydayBonus)
}

fun CousinMenuDay.oneDayMenu(everydayBonus: String) = this.oneDayMenu(item(everydayBonus))
fun CousinMenuDay.oneDayMenu() = this.oneDayMenu(Meal.заглушка.toCousinMenuItem())

data class CousinMenuItem(val name: String, val components: Map<Products, Int>) {
    fun toPFCC() = PFCC(
        components.entries.map { it.key.proteins * it.value / 100 }.sum(),
        components.entries.map { it.key.fats * it.value / 100 }.sum(),
        components.entries.map { it.key.carbohydrates * it.value / 100 }.sum(),
        components.entries.map { it.key.calories * it.value / 100 }.sum()
    )

    fun weight() = components.values.sum()

    fun prettyPrint() =
        name + if (components.entries.isNotEmpty()) {
            " (" + (components.entries.sortedBy { it.key.group }
                .map { it.key.name + " " + it.value }
                .joinToString(", ")) + ")"
        } else ""
}

data class PFCC(val proteins: Float, val fats: Float, val carbohydrates: Float, val calories: Float) {
    operator fun plus(increment: PFCC): PFCC {
        return PFCC(
            proteins + increment.proteins,
            fats + increment.fats,
            carbohydrates + increment.carbohydrates,
            calories + increment.calories
        )
    }
}

data class BOMItem(val name: String, val weight: Int) {
    fun prettyPrint() = "$name: $weight"
}


fun day(name: String, first: String, second: String, third: String, pocket: String): CousinMenuDay {
    val firstItem = LibraryLoader.items[first]!!
    val secondItem = LibraryLoader.items[second]!!
    val thirdItem = LibraryLoader.items[third]!!
    val pocketItem = LibraryLoader.items[pocket]!!
    return CousinMenuDay(firstItem, secondItem, thirdItem, pocketItem, name)
}

fun List<BOMItem>.merge() = this.groupingBy { it.name }
    .fold(0, operation = { accumulator: Int, element: BOMItem ->
        accumulator + element.weight
    })
    .map { entry -> BOMItem(entry.key, entry.value) }
    .sortedBy { it.name }

fun List<BOMItem>.sortByProductOrder() = this.sortedBy { Products.valueOf(it.name).group.order }
fun List<BOMItem>.mapToPrettyPrint() = this.map { it.prettyPrint() }
fun List<BOMItem>.prettyPrintWithSeparator(separator: CharSequence) = this.mapToPrettyPrint().joinToString(separator)
fun List<BOMItem>.prettyPrintln() = this.prettyPrintWithSeparator("\n")

enum class ProductCategory(val order: Int, val russian: String) {
    Meat(0, "Мясо"), Milk(1, "Сыры и молочное"), Grains(
        2,
        "Крупы"
    ),
    Vegetables(3, "Овощи и фрукты"), Nuts(4, "Орехи и сухофрукты"), Sweets(5, "Сладости"), Drinkable(
        6,
        "Чай и иные напитки"
    ),
    Dispersed(7, "Приправы, специи, соусы"),
    TouristShop(8, "Туристический отдел"),
    SportShop(8, "Спортивный магазин"),
    Whatever(99, "ХЗ")
}

fun Map<ProductCategory, String>.prettyPrint() =
    this.entries.sortedBy { it.key.order }.map { it.key.russian + ": " + it.value }.joinToString("\n")

fun item(name: String) = LibraryLoader.items[name]!!

fun menu(name: String, everydayBonus: String, vararg days: CousinMenuDay): CousinMenu {
    val item = item(everydayBonus)
    return CousinMenu(name, days.asList(), item)
}

fun Meal.toCousinMenuItem() = CousinMenuItem(this.russianName, this.components)