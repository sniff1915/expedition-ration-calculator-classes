package solutions.untested.food

enum class Meal(val russianName: String, val components: Map<Products, Int>) {
    заглушка(" ", mapOf()),
    уха(
        "финская уха Лохикейто",
        mapOf(
            Products.Лосось to 60,
            Products.`Картофельное пюре` to 60,
            Products.Лук to 10,
            Products.Морковь to 10,
            Products.Сливки to 4,
            Products.`Масло топленое` to 5,
            Products.Сухари to 15,
            Products.Печенье to 35
        )
    ),
    пшёнкасгущёночная(
        "пшёнка со сгущёнкой",
        mapOf(
            Products.Пшено to 80,
            Products.`Молоко сгущённое` to 20,
            Products.Морковь to 10,
            Products.Вафли to 30
        )
    ),
    собасговядинойкронидов(
        "соба с говядиной кронидов",
        mapOf(
            Products.`Лапша Соба Гречневая` to 75,
            Products.`Тушенка говядина Кронидов` to 80,
            Products.Пряники to 60,
            Products.Козинак to 60
        )
    ),
    собасгкурятинойкронидов(
        "соба с курятиной кронидов",
        mapOf(
            Products.`Лапша Соба Гречневая` to 75,
            Products.`Тушенка курятина Кронидов` to 80,
            Products.Пряники to 60,
            Products.Козинак to 60
        )
    )
}
