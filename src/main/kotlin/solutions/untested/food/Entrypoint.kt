package solutions.untested.food

class CousinProcessor

fun main(args: Array<String>) {
    val нет = " "
    val e = "продукты на все дни"
    val перекус = "перекус пбл"
    val перекус2 = "перекус2"
//    val `булгур и уха` = day(        "пшёнка-булгур-уха",        "пшено с курятиной кронидов",        "булгур с говядиной кронидов",        "финская уха Лохикейто",        нет    )
    val sniff1 = day("кускус", "кускус с сыром и сублимированными овощами", нет, нет, перекус)
    val sniff2 = day("рис-булгур", "рис с лососем", "булгур с грудкой" , нет, перекус)
    val sniff3 = day("перловка-гречка", "перловка с говядиной кронидов", "гречка с курятиной кронидов", нет, перекус)
    val sniff4 = day("рис-гречка-кускус ", "рис с кальмаром", "булгур с грудкой", нет, перекус)
    val sniff5 = day("перловка-рис", "перловка с курятиной кронидов", "рис с изюмом", нет, перекус)
    val sniff6 = day("перловка-рис", "перловка с говядиной кронидов", "рис с курятиной кронидов", нет, перекус)
    val sniff7 = day("гречка-булгур", "гречка с говядиной кронидов", "булгур с курятиной кронидов", нет, перекус)
    val sniff8 = day("перловка-соба", "перловка с курятиной кронидов", "соба с сыром", нет, перекус)
    val sniff9 = day("гречка-булгур", "гречка с сублями", "булгур с сублями", нет, перекус)
    val sniff10 = day("рис-соба", "рис с говядиной кронидов", "соба с курятиной кронидов", нет, перекус)
    val sniff11 = day("перловка", "перловка с говядиной кронидов", нет, нет, перекус)

    val `рис тушёнка, кускус с тушней` =
        day("рис-перловка", "рис с говядиной кронидов", "перловка с курятиной кронидов", нет, перекус)
    val `гречнево-булгур с тушёнкой` =
        day("булгур-греча, тушёное", "булгур с говядиной кронидов", "гречка с курятиной кронидов", нет, перекус)
    val `соба-перловый с тушёнкой` =
        day("соба-перлова, тушёное", "соба с говядиной кронидов", "перловка с курятиной кронидов", нет, перекус)
    val `соба-кускус с тушёнкой` =
        day("соба-кускус, тушёное", "соба с говядиной кронидов", "кускус с говядиной кронидов", нет, перекус)
    val `кускус, булгур с тушёнкой` =
        day("кускус-булгур", "кускус с говядиной кронидов", "булгур с говядиной кронидов", нет, перекус)
    val `кускус, гречка` =
        day("кускус-гречка", "кускус с говядиной кронидов", "гречка с курятиной кронидов", нет, перекус)

    val `трое в лодке и ни одной кошки` = menu(
        "П1-21-08",
        e,
        `рис тушёнка, кускус с тушней`,
        `соба-перловый с тушёнкой`,
        `кускус, булгур с тушёнкой`,
        `гречнево-булгур с тушёнкой`,
        `соба-кускус с тушёнкой`,
        `кускус, гречка`
    )

    val firstEvening = menu(
        "Первый вечер у входа", e,
        `соба-перловый с тушёнкой`
    )

    val group1underground = menu(
        "смена 1 пещерное 28-29, 29-30, 30-31", e,
        sniff2,
        sniff3,
        sniff4
    )

    val group1overground = menu(
        "вышли и сидим с 31 на 1", e,
        sniff2
    )

    val dmitriyAndIvanWaitingForGroup2 = menu(
        "дмитрий и иван ждут вторую группу с 1 на 2", e,
        sniff3
    )

    val dmitriyAndAlexanderWaitingTitov = menu(
        "сидим с Александром, ждём титова", e,
        sniff4
    )

    val firstPartOfGroup2underground = menu(
        "авангард второй смены уже ночует в ПБЛ", e,
        sniff2
    )

    val menuForAllteam2underground = menu(
        "меню для всей группы 2 под землёй", e,
        sniff2,
        sniff3,
        sniff4,
        sniff5,
        sniff6,
        sniff7,
        sniff8,
        sniff9,
        sniff10,
        sniff11,
    )

    val allMenus = listOf(
        firstEvening.wrapPair(4),
        group1underground.wrapPair(5),
        group1overground.wrapPair(5),
        dmitriyAndIvanWaitingForGroup2.wrapPair(2),

//        Pair(firstPartOfGroup2underground, 4),
//        Pair(menuForAllteam2underground, 7),

//        Pair(dmitriyAndAlexanderWaitingTitov, 1)
    )

    val sum: Int = allMenus.map {
        println("Секция меню \"${it.first.name}\": веса дней (${it.first.weights()}), общий вес ${it.first.weight(it.second)}")
        val testMenuStayingHome = CousinRuleEngine().testMenuStayingHome(it.first)
        println("Претензий насчитали: $testMenuStayingHome")
        println("Сама раскладка в .prettyPrint()")
        println(it.first.prettyPrint())
        testMenuStayingHome
    }.sum()

    println("Претензий насчитали: $sum")

    println()
    println("Вот BOM отдельно:")
    allMenus.forEach {
        println(it.first.name + " " + it.first.BOM(it.second).mapToPrettyPrint())
    }

    println("Объединим и выведем в сортировке по группам")
    val joined = allMenus
        .flatMap { it.first.BOM(it.second) }
        .merge()

    println(joined.sortByProductOrder().prettyPrintln())

    println("Вес всей раскладки: ${allMenus.map { it.first.weight(it.second) }.sum()}")
//    println("Вес трансуемой раскладки: ${pbl1.weight(4) + oneDaySoloMenu.weight(1) + pbl2.weight(2)}")

}
